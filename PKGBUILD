# Maintainer: Dan Johansen <strit@manjaro.org>
# Contribotor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Yosef Or Boczko <yoseforb@gnome.org>
# Contributor: Danct12 <danct12@disroot.org>

pkgname=gnome-software-mobile
pkgver=41.4
pkgrel=1
pkgdesc="GNOME Software Tools - Purism fork"
url="https://source.puri.sm/Librem5/gnome-software"
arch=(x86_64 armv7h aarch64)
license=(GPL2)
makedepends=(appstream gsettings-desktop-schemas libpackagekit-glib flatpak
             fwupd docbook-xsl git gobject-introspection gspell gtk-doc meson
             valgrind gnome-online-accounts libxmlb malcontent libhandy cmake
             sysprof)
source=(https://gitlab.gnome.org/GNOME/gnome-software/-/archive/$pkgver/gnome-software-$pkgver.tar.bz2
        0001-Revert-Launch-software-properties-gtk-in-place-of-the-sources-dialog.patch
        https://source.puri.sm/Librem5/pureos-store/-/raw/72eba88920417e9447ca8be5542aabd868aa0662/debian/patches/GsUpdatesSection-Always-present-Download-button-before-Re.patch)
sha256sums=('45c0f56c506b23644f0dacc267a5fe0104666dbfb787f38bfdbcc944ff3c69c7'
            'd454459d5b749f06da05a5a05cc7dad4097cc2c68297e91ead3126f05b007036'
            '16e3ce9de7ce22935352c59d517322c295a5a3e30a710c34e0e1bbab48d3ad9a')

prepare() {
  cd gnome-software-$pkgver
  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done
}

build() {
  arch-meson gnome-software-$pkgver build
  ninja -C build
}

package() {
  provides=(gnome-software gnome-software-packagekit-plugin)
  conflicts=(gnome-software gnome-software-packagekit-plugin)
  depends=(libxmlb gsettings-desktop-schemas gspell libpackagekit-glib
           gnome-online-accounts appstream libhandy sysprof
           archlinux-appstream-data gnome-software packagekit)
  optdepends=('flatpak: Flatpak support plugin'
              'fwupd: fwupd support plugin'
              'ostree: OSTree support plugin')

  DESTDIR="$pkgdir" meson install -C build
}
